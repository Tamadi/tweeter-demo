var gif = Vue.component('gif-search', {
	template: `
		<div>
			<input 
			@keyup.enter="getGifs" 
			type="text" 
			v-model="searchQuery">
			<div class="preview-window">
				<img class="gif-preview" v-for="gif in searchResults" :src="gif.images.downsized.url" >
			</div>
		</div>
	`,
	data(){
		return {
			searchQuery: "",
			searchResults: []
		}
	},
	methods: {
		getGifs(){
			// alert(this.searchQuery);
			axios.get("http://api.giphy.com/v1/gifs/search?q=jesus&api_key=EvkDKuCVQyidHdLbQuAIP4Ps1nrA794t&limit=25")
			.then((response) => {
				// console.log(response);
				this.searchResults = response.data.data;
			});
		}
	}
});

var app = new Vue({
	el: "#app3"
});
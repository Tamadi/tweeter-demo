<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Like;

class LikeController extends Controller
{
    public function likeTweet($id)
    {
        // if product exists or is valid

        $this->handleLike('App\Tweet', $id);
        return redirect()->back();
    }

    public function handleLike($type, $id)
    {



        $existing_like = Like::withTrashed()->where('Likeable_Type', $type)->where('Likeable_Id', $id)->where('user_id', auth()->user()->id)->first();

        if (is_null($existing_like)) {
            Like::create([
                'user_id'       => auth()->user()->id,
                'likeable_id'   => $id,
                'likeable_type' => $type,
            ]);
        } else {
            if (is_null($existing_like->deleted_at)) {
                $existing_like->delete();
            } else {
                $existing_like->restore();
            }
        }

    }

}

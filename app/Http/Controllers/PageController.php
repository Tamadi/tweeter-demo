<?php

namespace App\Http\Controllers;

use App\Tweet;
use App\User;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.index');
    }
    public function show()
    {
       $tweets = Tweet::all();
       $user = Auth::user()->id;
       $data['users']=$user;
       $data['tweets']=$tweets;
       return view('social');
    }
    public function info()
    {
        return view('pages.profile');
    }
    public function profile($id)
    {
        $user = Auth::user()->id;
        $profile = Profile::find($id);
        $data['users']=$user;
        $data['user_profiles']=$profile;
        $profile->user_id =$request->input('user_id');

        return view('pages.profile', $data);
    }
    public function about()

    {
        $tweets = Tweet::all();
        $user = Auth::user()->id;
        $data['users']=$user;
        $data['tweets']=$tweets;

        // dd($post);
        //$data['comments']= $tweet->comments;
        //$data['tweet'] = $tweet;

        return view('pages.notifications', ['tweets' => $tweets]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view ('pages.profile');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

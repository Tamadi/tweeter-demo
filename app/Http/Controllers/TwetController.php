<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TwetController extends Controller
{
    public function index() 
    { 
      // echo "test";
      // $user = Auth::user()->id;    
      // $tweets = Tweet::where('user_id','=',$user)->get(['body', 'retweets', 'likes']);    
      // return view('social');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $tweet = new Tweet ;
      $tweet->body = $request->body;
      $tweet->user_id = Auth::user()->id;     
      $tweet->save($tweet);     
      return redirect()->back();
    }
}

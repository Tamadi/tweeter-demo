<?php

namespace App\Http\Controllers;

use \App\Tweet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     *
     **/
    public function index()
    {

        $tweets = Tweet::with('likes', 'comments')->get();
        $user = Auth::user();
        $data['tweets']=$tweets;
        $data['user']=$user;

        //dd($tweets);
        return view('social', $data);
    }
}

<?php 

namespace App\Http\Controllers;

use App\Tweet;
use App\User;
use App\Like;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class TweetController extends Controller
{
    public function index() 
    {
          // echo "test";
      $user = Auth::user()->id;
      $tweets = Tweet::where('user_id','=',$user)->orderBy('updated_at', 'asc')->get(['body', 'retweets', 'likes']);
      // $data['tweets']=$tweets;
        return view('social');
    }

    public function create ()
    {
        return view('tweets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $user = User::find(Auth::user()->id);
      $tweet = new Tweet;
      // dd($user);
      // $tweet = new \App\Tweet ;
      $tweet->body = $request->body;
      $tweet->user_id = Auth::user()->id;
      $tweet->save(); 
      return redirect()->back();
    }

    public function show($id)
    {

      $tweet = tweet::find($id);

      $data['comments'] = $tweet->comments;

      $data['tweet']=$tweet;

      return view('tweets.show', $data);

    }

    public function edit($id)
    {
      $tweet = Tweet::find($id);

      $data['tweet'] = $tweet;

      return view('tweets.edit', $data);
    }

    public function update(Request $request, $id)
    {
        //dd($request);

        $validator = Validator::make($request->all(), [
            'body' => 'required',
        ]);

        if ($validator->fails()) {
          return redirect('/tweet/'.$id.'/edit')
                      ->withErrors($validator)
                      ->withInput();
        }

        $tweet = Tweet::find($id);


        // $tweet = new Tweet;
        // $tweet->title = $request->input('title');
        $tweet->body = $request->input('body');

        if ($tweet->save()) {
            return redirect ()->back();
        } 
    }
    public function delete($tweet_id)
    {
      $tweet = Tweet::where('id',$tweet_id)->first();
      $tweet->delete();
      return redirect()->route('home')->with(['message'=>'sucessfully delete']);
    }
    public function storeComment(Request $request)
    {
        $comment = new \App\Comment;
        $comment->user_id = Auth::user()->id;
        $comment->tweet_id =$request->input('tweet_id');
        $comment->body=$request->input('body');

        $comment->save();

        return redirect('/home/'.$request->input('tweet_id'));
    }
  //
}
  

<?php

namespace App\Http\Middleware;
use illuminate\Support\Facades\Auth;

use Closure;

class isAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guest()){
            return redirect('/');
            }
        return $next($request);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model 
{
	
    protected $fillable = [
        'user_id', 'body','likes','retweets',
    ];

  	public function user()

  	{
  		return $this->belongsTo('App\User');
	   }
	public function comments()
    {
    	return $this->hasMany('\App\Comment');
    }
  public function getcomments()
  {
    $comment = $this->comments()->where('tweet_id','body')->first();
    return($comments->body);
  }
  
  public function likes()
    {
        return $this->morphToMany('App\User', 'likeable')->whereDeletedAt(null);
    }

    public function getIsLikedAttribute()
    {
        $like = $this->likes()->where('likeable_id', 'user_id')->first();
          return (!is_null($like)) ? true : false;
    }

}
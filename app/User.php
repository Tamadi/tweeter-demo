<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table='users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username',
    ];

    // protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function tweets()
    {
        return $this->hasMany('App\Tweet', 'user_id', 'id');
    }
    public function comments()
    {
        return $this->hasMany('\App\Comment');
    }

    public function follows() 
    {
        return $this->hasMany(Follow::class);
    }
    public function isFollowing($target_id)
    {
        return (bool)$this->follows()->where('target_id', $target_id)->first(['id']);
    }
    public function likes()
    {
        return $this->belongsToMany('App\Tweet', 'likes', 'user_id', 'tweet_id');
    }
    public function likedTweets()
    {
        return $this->morphedByMany('App\Tweet', 'likeable')->whereDeletedAt(null);
    }
    public function profile()
    {
        return $this->hasOne('App\Profile');
    }
    
}
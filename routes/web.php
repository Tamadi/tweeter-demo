<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing', ['name'=> 'Weeter for Women']);
});
// Route::get('/welcome', function () {
//     return view('home', ['name' => 'Weeter for Women']);
// });
Route::post('/user', 'UserController@store');

Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'PageController@info');

Route::get('user/{id}', 'UserController@show');

Route::get('/show', 'PageController@show');
Route::get('/profile/{id}/edit', 'PageController@profile');
Route::get('/notify', 'PageController@about');

//Route::get('show/{profileId}/follow', 'ProfileController@followUser')->name('user.show');

//Route::post('profile/{profileId}/follow', 'ProfileController@followUser')->name('user.follow');
//Route::post('/{profileId}/unfollow', 'ProfileController@unFollowUser')->name('user.unfollow');

Route::get('/create', 'TweetController@create');
Route::get('/tweets', 'TweetController@index');
Route::get('/delete-tweet/{tweet_id}', 'TweetController@delete')->name('tweet.delete')->middleware('auth');
Route::get('tweet/{id}/edit', 'TweetController@edit');
Route::get('/tweet/{id}', 'TweetController@show');


//Route::get('/newtweet', 'TweetController@store');
//Route::get('/tweet', 'TweetController@index');
Route::post('/tweet/{id}', 'TweetController@update');
Route::post('/tweet', 'TweetController@store')->name('new.tweet');
// Route::post('/tweet/{id}, TweetController@index');
Route::post('/comment', 'TweetController@storeComment');
Route::group(['middleware' => ['auth']], function () 
{
    Route::get('/users', 'FollowController@index');
    Route::post('/follow/{user}', 'FollowController@follow');
    Route::delete('/unfollow/{user}', 'FollowController@unfollow');
});

Route::get('product/like/{id}', ['as' => 'product.like', 'uses' => 'LikeController@likeProduct']);
Route::get('tweet/like/{id}', ['as' => 'tweet.like', 'uses' => 'LikeController@likeTweet']);

@extends('layout.master')


@section('content')
<section>
		<div class="container" style="margin-bottom: 25px;">
			<div class="media-body">				
				<p class="pull-left">{{ $tweet->body }}</p>
				<div>
				<p> <ul>
					<ul><a href="{{ url('/tweets/') }}">Back to All tweet</a></ul>
					<ul><a href="{{ url('/home/') }}">Create a tweet</a></ul>
				</ul>
				</p>
				</div>

			<div class="panel panel-info" style="margin-bottom: 25px;">
				@foreach($comments as $comment)
				<div>
					<p>Author:{{$comment->user->username}}</p>
				</div> <br>
				<p>{{ $comment->body}}</p> <br>
				@endforeach
			</div>
		</div>
		@guest	
			<p>Please log in to comment</p>
			@else
			<form action="{{ url('/comment') }}" method="post">
				@csrf
				<input type="hidden" name="tweet_id" value="{{ $tweet->id }}"><br>
				 <div id="app3" class="container">
            		<gif-search></gif-search>
        		</div>
				<textarea name="body"></textarea><br>

				<button class="btn btn-primary" type="submit" value="Update">Submit Comment</button>
			</form>
		</div>
	@endguest
<script src="{{('js/app3.js')}}" ></script>
</section>

@endsection
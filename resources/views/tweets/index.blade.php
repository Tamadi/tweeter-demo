@extends('layout.master')

@section('content')

<section>

	<div class="fh5co-about animate-box">
		<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
			<h2>All Tweets</h2>			
		</div>	

		@foreach($tweets as $tweet)
		<div class="container" style="margin-bottom: 20px;">
			<div class="col-md-8 col-md-offset-2 animate-box">
				<small class="pull-left" style="position: relative; top: 12px; left: 5px;">
					<a href="{{ url( '/tweet/'.$tweet->id ) }}">View Tweet</a>
				</small>
				@guest
				@else
				<small class="pull-left" style="position: relative; top: 12px; left: 5px; margin-left: 5px;">
					<a href="{{ url( '/tweet/'.$tweet->id.'/edit' ) }}">&nbsp Edit Tweet</a>
				</small><br>
				@endguest
				<div class="container" style="margin-bottom: 12px;">
					<p class="pull-left">{{ $tweet->body }}</p><br><br>
				</div>
			</div>
		</div>
		@endforeach

	</div>
	
	
</section>

@endsection
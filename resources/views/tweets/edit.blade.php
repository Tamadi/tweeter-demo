@extends('layout.master')

@section('content')
<div class="media-body">
	<form class="form-group has-feedback" method="POST" action="{{ url('/tweet/'.$tweet->id) }}">
	@csrf
		<label class="control-label sr-only" for="inputSuccess5">Hidden label</label>
		<input type="text" class="form-control" id="search2" aria-describedby="edit-tweet" name="body">{{ ( old('body') ) ? old('body') : $tweet->body }}">
		@if($errors->has('body'))
		<span class="invalid-feedback" role="alert">
			<strong>{{
				$errors->first('body')
			}}</strong>
		</span>
		@endif
		<span class="glyphicon glyphicon-camera form-control-feedback"aria-hidden="true"></span>
		<span id="search2" class="sr-only">(success)</span>
		<button class="btn btn-primary" type="submit" aria-label="Left Align"name="submit" value="Update">
		<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>Update
		</button>
		<input type="hidden" value="{{Session::token()}}" name="_token">
	</form>
</div>

@endsection

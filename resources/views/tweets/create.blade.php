@extends('layout.master')

@section('content')
<div class="container" style="margin-bottom: 30px;">
	<div class="row">
		<h2>Create Tweet</h2>
		<div class="col-md-9 col-padded-right">
			<form method="post" action="{{ route('new.tweet') }}">
				@csrf	
			</div>
			<div class="form-group row">
				<div class="col-md-12 field">
					<label for="message">Tweet Body</label>
					<textarea class="form-control" id="message" cols="30" rows="10" name="body">{{ old('body') }}</textarea>
					@if ($errors->has('body'))
						<span class="invalid-feedback" role="alert">
						<strong>{{
							$errors->first('body')
						}}</strong>
						</span>
					@endif
				</div>
			</div>
			<div class="form-group row">
				<div class="col-md-12 field">
					<input type="submit" id="submit" class="btn btn-primary" value="Tweet">
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
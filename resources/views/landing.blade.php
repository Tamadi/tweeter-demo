<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.css">
	<title>{{ $name }}</title>
</head>
<body>
<section class="section">
  <!-- Logo area on Nav -->
  <nav class="navbar" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" href="#">
      <img src="images/brush-small.png" width="112" height="52">
    </a>
<!-- Hamburger menu -->
    <a role="button" class="navbar-burger burger is-mobile" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>
<!-- Navigation Menu -->
  <div id="navbarBasicExample" class="navbar-menu custom-navbar is-mobile">
    <div class="navbar-start">
      <a class="navbar-item smoothScroll" href="#Home">
        Home
      </a>

      <a class="navbar-item smoothScroll" href="#About">
        About
      </a>

      <a class="navbar-item smoothScroll" href="#Features">
        Features
      </a>
      <a class="navbar-item smoothScroll" href="#Benefits">
        Benefits
      </a>

    </div>

    <div class="navbar-end">
      <div class="navbar-item">
        <div class="buttons">
          <a class="button is-primary" href="{{ url('/register')}}">
            <strong>Register</strong>
          </a>
          <a class="button is-light" href="{{ url('/login')}}">
            Log in
          </a>
        </div>
      </div>
    </div>
  </div>
</nav>
</section>
  <!-- Home Text head -->
  <section class="hero is-primary is-small" id="Home">
  <div class="hero-body">
    <div class="container">
      <h1 class="title has has-text-centered">
        Welcome to Weeter for Women
      </h1>
      <h2 class="subtitle has-text-centered">
        A place to Grow, Connect, Share and Collaborate
      </h2>
    </div>
  </div>
</section>
<!-- Home intro and image -->
<section>
  <div class="has-text-centered">
          <figure class="image">
              <img src="images/virtual-workspace.jpg" style="max-height: 600px;">
          </figure>
          <div style="text-align:center;line-height:0; position:relative;">
            <button style="z-index:5;" value="{{url('/register')}}" class="button is-danger is-medium">Sign Up</button>
          </div>        
  </div>
</section>
<section class="hero is-info is-small" id="About">
  <div class="hero-body">
    <div class="container">
      <h1 class="title has-text-centered">
        About
      </h1>
      <h2 class="subtitle has-text-centered">
        Weeter for Women
      </h2>
    </div>
  </div>
</section>
<!-- about page content -->
<section>
  <div class="container has-text-centered is-light">
    <h2 style="text-align: center; font-size: 35px;">
      Weeter for Women aims to bring the world closer, by aiding in growth, collaboration and more. Imagine the world of women at your finger tips and the ability to use your gifts to network and connect with other business owners and moms. That's what Weeter is all about. Reach out to other mom's, start a new business and advertise or simply drop a inspirational note. Do it all while connecting virtually. The world is closer than you think!
    </h2>
  </div>
</section>
<section class="hero is-success is-small" id="Features">
  <div class="hero-body">
    <div class="container">
      <h1 class="title has-text-centered">
        Features
      </h1>
      <h2 class="subtitle has-text-centered">
        Likes, Shares, Comments and Much more...
      </h2>
    </div>
  </div>
</section>
<!-- Features Content -->
<section class="section">
  <div class="container">
    <div class="notification columns">
      <div class="column">
        <h2 class="subtitle has-text-centered">
          <i class="fab fa-twitter"></i>
          Tweets
        </h2>
        <div class="notification has-text-centered">
          Create tweets that your circle or friends can view and comment on
        </div>
      </div>
    <div class="column">
      <h2 class="subtitle has-text-centered">
        <i class="fas fa-thumbs-up"></i>
        Likes
      </h2>
      <div class="notification has-text-centered">
        Like yours or other peoples tweets according to your preference 
      </div>
    </div>
    <div class="column">
      <h2 class="subtitle has-text-centered">
        <i class="fas fa-retweet"></i>
        Share and retweet
      </h2>
      <div class="notification has-text-centered">
        Share tweets from businesses and other retweets
      </div>
    </div>
    <div class="column">
      <h2 class="subtitle has-text-centered">
        <i class="fas fa-users"></i>
        Follow
      </h2>
      <div class="notification has-text-centered">
        Follow and unfollow users on Weeter
      </div>
    </div>
    <div class="column">
      <h2 class="subtitle has-text-centered">
        <i class="fas fa-bell"></i>
        Notifications
      </h2>
      <div class="notification has-text-centered">
        View real time notifications of your circles latest activity. 
      </div>
    </div>
  </div>
</div>
  
</section>
<section class="hero is-warning is-small" id="Benefits">
  <div class="hero-body">
    <div class="container">
      <h1 class="title has-text-centered">
        Benefits
      </h1>
      <div class="has-text-centered">
        Bring the world closer to your finger tips
      </div>
    </div>
  </div>
</section>
<!-- Benefits content -->
<section></section>
<section class="footer is-small is-light has-text-centered">
  <a href="#" class="is-danger">&copy Weeter for Women 2019</a>
</section>
<script src="js/jquery.js"></script>
<script src="js/smoothscroll.js"></script>
<!-- <script src="js/scrollmagic/minified/ScrollMagic.min.js"></script> -->
<script src="js/jquery.parallax.js"></script>
<!-- <script src="js/scrollmagic/uncompressed/plugins/debug.addIndicators.js"></script> -->
<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
<script src="js/wow.min.js"></script>
<script type="text/javascript" src="js/custom.js">
  
</script>
</body>
</html>

       @foreach($tweets as $tweet)
        <div class="container" style="margin-bottom: 20px;">
            <div class="media-body">
                <h4 class="media-left">{{ $user->username}}</h4><br>
                @guest
                @else
                <small class="pull-left" style="position: relative; top: 5px; left: 5px; margin-left: 10px;">
                    <a href="{{ route('tweet.delete', ['tweet_id'=>$tweet->id] ) }}">&nbsp Delete</a>
                </small><br>
                @endguest
                <div class="col-sm-8">
                    <div class="card-body" style="margin-bottom: 12px;">
                        <div class="pull-left has-feedback col-sm-4">{{ $tweet->body }}</div><br>
                        <ul class="nav nav-pills nav-pills-custom">
                             <li><a href="{{ url( '/tweet/'.$tweet->id ) }}" style="color:#B88275;"><span class="glyphicon glyphicon-share-alt"></span></a></li>
                            <li><a href="#" style="color:#B88275;"><span class="glyphicon glyphicon-retweet"></span> &nbsp{{ $tweet->retweets }}</a></li>
                            
                            <li><a href="{{route('tweet.like',['user_id'=>$tweet->id])}}"  style="color:#B60001;"> @if($tweet->likes)<span class="glyphicon glyphicon-star-empty"></span></a></li>
                            @else
                            <li><a href="{{route('tweet.like',['user_id'=>$tweet->id])}}"  style="color:#B88275;"> <span class="glyphicon glyphicon-star"></span></a></li>
                            @endif
                               <li><a class="pull-right form-control" style="color:#B88275;">{{ $tweet->likes()->count()}}</a><li> 

                            <li><a href="{{ url('/tweet/'.$tweet->id.'/edit' ) }}" data-toggle="popover" title="Edit" style="color:#B88275;"><span class="glyphicon glyphicon-option-horizontal"></span></a></li>
                        </ul>
                            <div class="pull-left has-feedback col-sm-3"></div><br>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
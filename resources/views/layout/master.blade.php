<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Weeter for Women</title>
  
 	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

  <link rel='stylesheet' href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'>

      <link rel="stylesheet" href="css/style2.css">

  
</head>

<body>

@include('layout.nav')

@yield('content')

@include('layout.footer')
<script src="node_modules/axios/dist/axios.js"></script>
<script src="node_modules/vue/dist/vue.js"></script>
<script src="../js/app3.js"></script>
</body>
</html>

<div class="navbar navbar-default navbar-static-top">
	<div class="container">
		<div class="navbar-collapse navbar-collapse-1 collapse" aria-expanded="true">
			<ul class="nav navbar-nav">
				<li class="active">
					<a href="{{('home')}}"><span class="glyphicon glyphicon-home"></span> Home</a>
				</li>
				<li>
					<a href="{{ url('/notify') }}"><span class="glyphicon glyphicon-bell"></span> Notifications</a>
				</li>
				<li>
    				<a href="{{('home') }}"><span class="glyphicon glyphicon-pencil"></span>New Tweet</a>
    			</li>
    			<li>
    				<a href="{{ url('/users') }}"><span class="glyphicon glyphicon-user"></span>Users</a>
    			</li>
			</ul>
			<div class="navbar-form navbar-right">
				<div class="form-group has-feedback">
					<input type="text" class="form-control-nav" id="search" aria-describedby="search1">
					<span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
				</div>

				<button class="btn btn-primary" type="submit" aria-label="Left Align">
					<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Tweet
				</button>
			</div>
		</div>
	</div>
</div>
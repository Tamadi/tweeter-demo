@extends(layout.master)

@section('content')

<h1> Fill Out Your Profile</h1>
<form method="POST" action="">
	@csrf
	 <div class="form-group">
	 
	    <label>Username</label>
	    <input name="username" type="text" class="form-control" value="{{ $user->username }}" />{{ (old('username') ) ? old('username') : $user->username}}
	    @if ($errors->has('username'))
			<span class="invalid-feedback" role="alert">
				<strong>{{
					$errors->first('username')
				}}</strong>
				</span>
		@endif
	</div>
	<div class="form-group">
	    <label>Email</label>
	    <input name="email" type="email" class="form-control" value="{{ (old('email') ) ? old('email') : $user->email }}" />
	    @if ($errors->has('body'))
			<span class="invalid-feedback" role="alert">
				<strong>{{
					$errors->first('email')
				}}</strong>
				</span>
		@endif
	</div>
	<div class="form-group">
	    <label>Location</label>
	    <input name="location" type="text" class="form-control" value="{{ (old('location') ) ? old('location') : $user->location}}"/>
	    @if ($errors->has('location'))
			<span class="invalid-feedback" role="alert">
				<strong>{{
					$errors->first('location')
				}}</strong>
			</span>
		@endif
	</div>
	<div class="form-group">
	    <label>Bio (about you)</label>
	    <textarea name="bio" type="text" class="form-control"> {{ (old('bio') ) ? old('bio') : $user->bio}}"</textarea>
	    @if ($errors->has('body'))
			<span class="invalid-feedback" role="alert">
				<strong>{{
					$errors->first('body')
				}}</strong>
			</span>
		@endif
	</div>
	<div class="form-group">
		<input type="submit" id="submit" class="btn btn-primary" value="Update">
	</div>

</form>
@endsection
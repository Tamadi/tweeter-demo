               <form class="login100-form validate-form" action="{{ route('register') }}" method="POST">
               @csrf
               <span class="login100-form-title p-b-59">
                  Sign Up
               </span>

               <div class="wrap-input100 validate-input" data-validate="Name is required">
                  <span class="label-input100">Full Name</span>
                  <input class="input100" type="text" name="name" placeholder="Name...">
                  <span class="focus-input100"></span>
                  @if ($errors->has('name'))
                     <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('name') }}</strong>
                   </span>
                  @endif
               </div>

               <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">

                  
                  <span class="label-input100">Email</span> 
                  <input class="input100" type="text" name="email" placeholder="Email addess..."></span>
                  <span class="focus-input100"></span>  
                     @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                         <strong>{{ $errors->first('email') }}</strong>
                      </span>
                     @endif
               </div>          

               <div class="wrap-input100 validate-input" data-validate="Username is required">
                  <span class="label-input100">Username</span>
                  <input class="input100" type="text" name="username" placeholder="Username...">
                  <span class="focus-input100"></span>
               </div>

               <div class="wrap-input100 validate-input" data-validate = "Password is required">
                  <span class="label-input100">Password</span>
                  <input class="input100" type="password" name="password" placeholder="*************">
                  <span class="focus-input100"></span>
                  @if ($errors->has('password'))
                     <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                     </span>
                  @endif

               </div>

               <div class="wrap-input100 validate-input" data-validate = "Repeat Password is required">
                  <span class="label-input100">Repeat Password</span>
                  <input class="input100" type="password" name="password_confirmation">
                  <span class="focus-input100"></span>
               </div>

               <div class="flex-m w-full p-b-33">
                  <div class="contact100-form-checkbox">
                     <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                     <label class="label-checkbox100" for="ckb1">
                        <span class="txt1">
                           I agree to the
                           <a href="#" class="txt2 hov1">
                              Terms of User
                           </a>
                        </span>
                     </label>
                  </div>

                  
               </div>

               <div class="container-login100-form-btn">
                  <div class="wrap-login100-form-btn">
                     <div class="login100-form-bgbtn"></div>
                     <button class="login100-form-btn" type="submit">
                       {{ __('Register') }}
                     </button>
                  </div>

                  <a href="((/login))" class="dis-block txt3 hov1 p-r-30 p-t-10 p-b-10 p-l-30">
                     Sign in
                     <i class="fa fa-long-arrow-right m-l-5"></i>
                  </a>
               </div>
            </form>
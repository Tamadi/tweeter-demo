<div class="col-sm-3">
	<div class="panel panel-default panel-custom">
		<div class="panel-heading">
			<h3 class="panel-title">
			Who to follow
			<small><a href="#">Refresh</a> ● <a href="{{route('user.show')}}">View all</a></small>
			</h3>
		</div>
		<div class="panel-body">
			<form method="POST"
				<div class="media">
					<div class="media-left">
						<img src="http://placehold.it/32x32" alt="" class="media-object img-rounded">
					</div>
					<div class="media-body">
						<h4 class="media-heading">Nome e cognome</h4>
						<a href="{{route('user.follow')}}" class="btn btn-default btn-xs">
							+
							<span class="glyphicon glyphicon-user"></span>
							Follow
						</a>
					</div>
				</div>
@extends('layout.master')

@section('title', 'Show All Tweets')

@section('content')

<h1>Showing All Tweets <h1>
	<div class="panel panel-info">
		<div class="panel-body">
			<div class="media-body">
				<p>{{$tweets}} </p><br><br>
			</div>
		</div>
	</div>

@endsection
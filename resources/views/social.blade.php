<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Weeter for Women</title>
  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

  <link rel='stylesheet' href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'>

      <link rel="stylesheet" href="css/style2.css">

  
</head>

<body>

  
<div class="navbar navbar-default navbar-static-top">
	<div class="container">
		<div class="navbar-collapse navbar-collapse-1 collapse" aria-expanded="true">
			<ul class="nav navbar-nav">
				<li class="active">
					<a href="{{('home')}}"><span class="glyphicon glyphicon-home"></span> Home</a>
				</li>
				<li>
					<a href="{{ url('/notify') }}"><span class="glyphicon glyphicon-bell"></span> Notifications</a>
				</li>
				<li>
    				<a href="{{('home') }}"><span class="glyphicon glyphicon-pencil"></span>New Tweet</a>
    			</li>
    			<li>
    				<a href="{{ url('/users') }}"><span class="glyphicon glyphicon-user"></span>Follow Users</a>
    			</li>
    			<li>
    				<a href="{{ url('/profile') }}"><span class="glyphicon glyphicon-user"></span>Profile</a>
    			</li>
			</ul>
			<form class="navbar-form navbar-right" method="POST" action="{{route('new.tweet')}}">
				<div class="form-group has-feedback">
						<input type="text" name="body" class="form-control-nav" id="search" aria-describedby="search1" placeholder="search">
						<span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
				</div>

				<button class="btn btn-primary" type="submit" aria-label="Left Align">
					<span class="glyphicon glyphicon-pencil" aria-hidden="true"> </span> Tweet
				</button>
				<input type="hidden" value="{{Session::token()}}" name="_token">
			</form>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-sm-3">
			<div class="panel panel-default">
				<div class="panel-body">
					<a href="#"><img class="img-responsive" alt="" src="http://placehold.it/800x500"></a>
					<div class="row">
						<div class="col-xs-3">
							<h5>
								<small>TWEETS</small>
								<a href="#">1,545</a>
							</h5>
						</div>
						<div class="col-xs-4">
							<h5>
								<small>FOLLOWING</small>
								<a href="#">251</a>
							</h5>
						</div>
						<div class="col-xs-5">
							<h5>
								<small>FOLLOWERS</small>
								<a href="#">153</a>
							</h5>
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-default panel-custom">
				<div class="panel-heading">
					<h3 class="panel-title">
						Trends
						<small><a href="#">ciao</a></small>
					</h3>
				</div>

				<div class="panel-body">
					<ul class="list-unstyled">
						<li><a href="#">#Cras justo odio</a></li>
						<li><a href="#">#Dapibus ac facilisis in</a></li>
						<li><a href="#">#Morbi leo risus</a></li>
						<li><a href="#">#Porta ac consectetur ac</a></li>
						<li><a href="#">#Vestibulum at eros</a></li>
						<li><a href="#">#Vestibulum at eros</a></li>
						<li><a href="#">#Vestibulum at eros</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="media">
						<a class="media-left" href="#fake">
							<img alt="" class="media-object img-rounded" src="http://placehold.it/35x35">
						</a>
						<div class="media-body">
							<form class="form-group has-feedback" method="POST" action="{{route('new.tweet')}}">
								<label class="control-label sr-only" for="inputSuccess5">Hidden label</label>
								<input type="textarea" class="form-control" id="search2" aria-describedby="search" name="body">
								<span class="glyphicon glyphicon-camera form-control-feedback" aria-hidden="true"></span>
								<span id="search2" class="sr-only">(success)</span>
								<button class="btn btn-primary" type="submit" aria-label="Left Align" name="submit" value="submit">
								<span class="glyphicon glyphicon-pencil" aria-hidden="true"> </span> Tweet
								</button>
								<input type="hidden" value="{{Session::token()}}" name="_token">
							</form>
						</div>
					</div>
				</div>
				<div class="panel-body">
					<div class="media">
						@include('member')
				</div>
			</div>

			<br>
			<br>
			<br>


			<div class="panel panel-default">
				<div class="panel-heading">Navigation</div>
				<div class="panel-body">
					<ul class="nav nav-pills">
						<li role="presentation" class="active"><a href="{ url('/')}}">Home</a></li>
						<li role="presentation"><a href="{{ url('/profile')}}">Profile</a></li>
						<li role="presentation"><a href="{{url('/notify')}}">Notifications</a></li>
						<li role="presentation"><a href="{{ url('/logout')}}">logout</a><li>
					</ul>
				</div>
			</div>
		</div> 

</body>

</html>